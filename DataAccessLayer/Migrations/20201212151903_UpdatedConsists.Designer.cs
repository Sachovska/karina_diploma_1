﻿// <auto-generated />
using System;
using DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DataAccessLayer.Migrations
{
    [DbContext(typeof(MainDbContext))]
    [Migration("20201212151903_UpdatedConsists")]
    partial class UpdatedConsists
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.0.0")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Models.Entities.CakeCakeBase", b =>
                {
                    b.Property<int?>("CakeBaseId")
                        .HasColumnType("int");

                    b.Property<int?>("CakeId")
                        .HasColumnType("int");

                    b.HasKey("CakeBaseId", "CakeId");

                    b.HasIndex("CakeId");

                    b.ToTable("CakeCakeBase");
                });

            modelBuilder.Entity("Models.Entities.CoatingCake", b =>
                {
                    b.Property<int?>("CakeCoatingId")
                        .HasColumnType("int");

                    b.Property<int?>("CakeId")
                        .HasColumnType("int");

                    b.HasKey("CakeCoatingId", "CakeId");

                    b.HasIndex("CakeId");

                    b.ToTable("CoatingCake");
                });

            modelBuilder.Entity("Models.Entities.CreamCake", b =>
                {
                    b.Property<int?>("CakeCreamId")
                        .HasColumnType("int");

                    b.Property<int?>("CakeId")
                        .HasColumnType("int");

                    b.HasKey("CakeCreamId", "CakeId");

                    b.HasIndex("CakeId");

                    b.ToTable("CreamCake");
                });

            modelBuilder.Entity("Models.Entities.DecorCake", b =>
                {
                    b.Property<int?>("CakeDecorationId")
                        .HasColumnType("int");

                    b.Property<int?>("CakeId")
                        .HasColumnType("int");

                    b.HasKey("CakeDecorationId", "CakeId");

                    b.HasIndex("CakeId");

                    b.ToTable("DecorCake");
                });

            modelBuilder.Entity("Models.Entities.FillingCake", b =>
                {
                    b.Property<int?>("CakeFillingId")
                        .HasColumnType("int");

                    b.Property<int?>("CakeId")
                        .HasColumnType("int");

                    b.HasKey("CakeFillingId", "CakeId");

                    b.HasIndex("CakeId");

                    b.ToTable("FillingCake");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.Cake", b =>
                {
                    b.Property<int>("idCake")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("aboutCake")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("dateOfCreating")
                        .HasColumnType("datetime2");

                    b.Property<string>("imgURL")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("nameCake")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("priceCake")
                        .HasColumnType("int");

                    b.Property<string>("status")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("weightCake")
                        .HasColumnType("int");

                    b.HasKey("idCake")
                        .HasName("PrimaryKey_idCake");

                    b.ToTable("Cakes");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.Client", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Address")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Login")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Password")
                        .HasColumnType("nvarchar(max)");

                    b.Property<long>("Phone")
                        .HasColumnType("bigint");

                    b.Property<int>("Prepayment")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("Clients");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.Delivery", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Price")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("Deliveries");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.Ingredient", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Measureid")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("Measureid");

                    b.ToTable("Ingredients");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.Measure", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Fullname")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Measures");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ClientID")
                        .HasColumnType("int");

                    b.Property<DateTime>("Dateofdelivery")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("Dateoforder")
                        .HasColumnType("datetime2");

                    b.Property<int>("DeliveryID")
                        .HasColumnType("int");

                    b.Property<int>("Discount")
                        .HasColumnType("int");

                    b.Property<int>("Number")
                        .HasColumnType("int");

                    b.Property<int>("Prepayment")
                        .HasColumnType("int");

                    b.Property<bool>("Prepaymenttrue")
                        .HasColumnType("bit");

                    b.Property<int>("Sum")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("ClientID");

                    b.HasIndex("DeliveryID");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.OrderBody", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Amount")
                        .HasColumnType("int");

                    b.Property<int>("CakeID")
                        .HasColumnType("int");

                    b.Property<int>("OrderID")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("CakeID");

                    b.HasIndex("OrderID");

                    b.ToTable("OrderBodies");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.OrderProgress", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("Dateofdelivery")
                        .HasColumnType("datetime2");

                    b.Property<int>("OrderID")
                        .HasColumnType("int");

                    b.Property<bool>("Ready")
                        .HasColumnType("bit");

                    b.HasKey("Id");

                    b.HasIndex("OrderID");

                    b.ToTable("OrderesProgress");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.cakeBase", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("IngredientId")
                        .HasColumnType("int");

                    b.Property<string>("name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("price")
                        .HasColumnType("int");

                    b.HasKey("id");

                    b.HasIndex("IngredientId");

                    b.ToTable("Biscuits");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.cakeCoating", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("imgURL")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("price")
                        .HasColumnType("int");

                    b.HasKey("id");

                    b.ToTable("Coatings");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.cakeCream", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("IngredientId")
                        .HasColumnType("int");

                    b.Property<string>("name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("price")
                        .HasColumnType("int");

                    b.HasKey("id");

                    b.HasIndex("IngredientId");

                    b.ToTable("Creams");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.cakeDecoration", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("IngredientId")
                        .HasColumnType("int");

                    b.Property<string>("imgURL")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("price")
                        .HasColumnType("int");

                    b.HasKey("id");

                    b.HasIndex("IngredientId");

                    b.ToTable("Decors");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.cakeFilling", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("IngredientId")
                        .HasColumnType("int");

                    b.Property<string>("name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("price")
                        .HasColumnType("int");

                    b.HasKey("id");

                    b.HasIndex("IngredientId");

                    b.ToTable("Fruits");
                });

            modelBuilder.Entity("Models.Entities.CakeCakeBase", b =>
                {
                    b.HasOne("ShopWebApp.Model.Entities.cakeBase", "CakeBase")
                        .WithMany("CakeBase")
                        .HasForeignKey("CakeBaseId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ShopWebApp.Model.Entities.Cake", "Cake")
                        .WithMany("CakeBase")
                        .HasForeignKey("CakeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Models.Entities.CoatingCake", b =>
                {
                    b.HasOne("ShopWebApp.Model.Entities.cakeCoating", "CakeCoating")
                        .WithMany("Coating")
                        .HasForeignKey("CakeCoatingId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ShopWebApp.Model.Entities.Cake", "Cake")
                        .WithMany("Coating")
                        .HasForeignKey("CakeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Models.Entities.CreamCake", b =>
                {
                    b.HasOne("ShopWebApp.Model.Entities.cakeCream", "CakeCream")
                        .WithMany("Cream")
                        .HasForeignKey("CakeCreamId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ShopWebApp.Model.Entities.Cake", "Cake")
                        .WithMany("Cream")
                        .HasForeignKey("CakeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Models.Entities.DecorCake", b =>
                {
                    b.HasOne("ShopWebApp.Model.Entities.cakeDecoration", "CakeDecoration")
                        .WithMany("Decor")
                        .HasForeignKey("CakeDecorationId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ShopWebApp.Model.Entities.Cake", "Cake")
                        .WithMany("Decor")
                        .HasForeignKey("CakeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Models.Entities.FillingCake", b =>
                {
                    b.HasOne("ShopWebApp.Model.Entities.cakeFilling", "CakeFilling")
                        .WithMany("Fruit")
                        .HasForeignKey("CakeFillingId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ShopWebApp.Model.Entities.Cake", "Cake")
                        .WithMany("Fruit")
                        .HasForeignKey("CakeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.Ingredient", b =>
                {
                    b.HasOne("ShopWebApp.Model.Entities.Measure", "Measure")
                        .WithMany("Ingredients")
                        .HasForeignKey("Measureid")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.Order", b =>
                {
                    b.HasOne("ShopWebApp.Model.Entities.Client", "Client")
                        .WithMany("Orders")
                        .HasForeignKey("ClientID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ShopWebApp.Model.Entities.Delivery", "Delivery")
                        .WithMany("Orders")
                        .HasForeignKey("DeliveryID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.OrderBody", b =>
                {
                    b.HasOne("ShopWebApp.Model.Entities.Cake", "Cake")
                        .WithMany("OrderBodies")
                        .HasForeignKey("CakeID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("ShopWebApp.Model.Entities.Order", "Order")
                        .WithMany("Orderbodies")
                        .HasForeignKey("OrderID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.OrderProgress", b =>
                {
                    b.HasOne("ShopWebApp.Model.Entities.Order", "Order")
                        .WithMany("Orderprogreses")
                        .HasForeignKey("OrderID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.cakeBase", b =>
                {
                    b.HasOne("ShopWebApp.Model.Entities.Ingredient", null)
                        .WithMany("Biscuits")
                        .HasForeignKey("IngredientId");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.cakeCream", b =>
                {
                    b.HasOne("ShopWebApp.Model.Entities.Ingredient", null)
                        .WithMany("Creams")
                        .HasForeignKey("IngredientId");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.cakeDecoration", b =>
                {
                    b.HasOne("ShopWebApp.Model.Entities.Ingredient", null)
                        .WithMany("Decors")
                        .HasForeignKey("IngredientId");
                });

            modelBuilder.Entity("ShopWebApp.Model.Entities.cakeFilling", b =>
                {
                    b.HasOne("ShopWebApp.Model.Entities.Ingredient", null)
                        .WithMany("Fruits")
                        .HasForeignKey("IngredientId");
                });
#pragma warning restore 612, 618
        }
    }
}
