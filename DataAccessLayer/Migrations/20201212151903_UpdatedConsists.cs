﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccessLayer.Migrations
{
    public partial class UpdatedConsists : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cakes",
                columns: table => new
                {
                    idCake = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nameCake = table.Column<string>(nullable: true),
                    imgURL = table.Column<string>(nullable: true),
                    aboutCake = table.Column<string>(nullable: true),
                    weightCake = table.Column<int>(nullable: false),
                    priceCake = table.Column<int>(nullable: false),
                    status = table.Column<string>(nullable: true),
                    dateOfCreating = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PrimaryKey_idCake", x => x.idCake);
                });

            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Phone = table.Column<long>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Prepayment = table.Column<int>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Login = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Coatings",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    price = table.Column<int>(nullable: false),
                    imgURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coatings", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Deliveries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Deliveries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Measures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Fullname = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Measures", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CoatingCake",
                columns: table => new
                {
                    CakeId = table.Column<int>(nullable: false),
                    CakeCoatingId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoatingCake", x => new { x.CakeCoatingId, x.CakeId });
                    table.ForeignKey(
                        name: "FK_CoatingCake_Coatings_CakeCoatingId",
                        column: x => x.CakeCoatingId,
                        principalTable: "Coatings",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoatingCake_Cakes_CakeId",
                        column: x => x.CakeId,
                        principalTable: "Cakes",
                        principalColumn: "idCake",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Number = table.Column<int>(nullable: false),
                    ClientID = table.Column<int>(nullable: false),
                    DeliveryID = table.Column<int>(nullable: false),
                    Dateoforder = table.Column<DateTime>(nullable: false),
                    Dateofdelivery = table.Column<DateTime>(nullable: false),
                    Sum = table.Column<int>(nullable: false),
                    Prepayment = table.Column<int>(nullable: false),
                    Prepaymenttrue = table.Column<bool>(nullable: false),
                    Discount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Clients_ClientID",
                        column: x => x.ClientID,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Deliveries_DeliveryID",
                        column: x => x.DeliveryID,
                        principalTable: "Deliveries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Ingredients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Measureid = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ingredients", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ingredients_Measures_Measureid",
                        column: x => x.Measureid,
                        principalTable: "Measures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderBodies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderID = table.Column<int>(nullable: false),
                    CakeID = table.Column<int>(nullable: false),
                    Amount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderBodies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderBodies_Cakes_CakeID",
                        column: x => x.CakeID,
                        principalTable: "Cakes",
                        principalColumn: "idCake",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderBodies_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderesProgress",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderID = table.Column<int>(nullable: false),
                    Dateofdelivery = table.Column<DateTime>(nullable: false),
                    Ready = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderesProgress", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderesProgress_Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Biscuits",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    price = table.Column<int>(nullable: false),
                    IngredientId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Biscuits", x => x.id);
                    table.ForeignKey(
                        name: "FK_Biscuits_Ingredients_IngredientId",
                        column: x => x.IngredientId,
                        principalTable: "Ingredients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Creams",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    price = table.Column<int>(nullable: false),
                    IngredientId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Creams", x => x.id);
                    table.ForeignKey(
                        name: "FK_Creams_Ingredients_IngredientId",
                        column: x => x.IngredientId,
                        principalTable: "Ingredients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Decors",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    price = table.Column<int>(nullable: false),
                    imgURL = table.Column<string>(nullable: true),
                    IngredientId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Decors", x => x.id);
                    table.ForeignKey(
                        name: "FK_Decors_Ingredients_IngredientId",
                        column: x => x.IngredientId,
                        principalTable: "Ingredients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Fruits",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true),
                    price = table.Column<int>(nullable: false),
                    IngredientId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fruits", x => x.id);
                    table.ForeignKey(
                        name: "FK_Fruits_Ingredients_IngredientId",
                        column: x => x.IngredientId,
                        principalTable: "Ingredients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CakeCakeBase",
                columns: table => new
                {
                    CakeId = table.Column<int>(nullable: false),
                    CakeBaseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CakeCakeBase", x => new { x.CakeBaseId, x.CakeId });
                    table.ForeignKey(
                        name: "FK_CakeCakeBase_Biscuits_CakeBaseId",
                        column: x => x.CakeBaseId,
                        principalTable: "Biscuits",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CakeCakeBase_Cakes_CakeId",
                        column: x => x.CakeId,
                        principalTable: "Cakes",
                        principalColumn: "idCake",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CreamCake",
                columns: table => new
                {
                    CakeId = table.Column<int>(nullable: false),
                    CakeCreamId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreamCake", x => new { x.CakeCreamId, x.CakeId });
                    table.ForeignKey(
                        name: "FK_CreamCake_Creams_CakeCreamId",
                        column: x => x.CakeCreamId,
                        principalTable: "Creams",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CreamCake_Cakes_CakeId",
                        column: x => x.CakeId,
                        principalTable: "Cakes",
                        principalColumn: "idCake",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DecorCake",
                columns: table => new
                {
                    CakeId = table.Column<int>(nullable: false),
                    CakeDecorationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DecorCake", x => new { x.CakeDecorationId, x.CakeId });
                    table.ForeignKey(
                        name: "FK_DecorCake_Decors_CakeDecorationId",
                        column: x => x.CakeDecorationId,
                        principalTable: "Decors",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DecorCake_Cakes_CakeId",
                        column: x => x.CakeId,
                        principalTable: "Cakes",
                        principalColumn: "idCake",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FillingCake",
                columns: table => new
                {
                    CakeId = table.Column<int>(nullable: false),
                    CakeFillingId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FillingCake", x => new { x.CakeFillingId, x.CakeId });
                    table.ForeignKey(
                        name: "FK_FillingCake_Fruits_CakeFillingId",
                        column: x => x.CakeFillingId,
                        principalTable: "Fruits",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FillingCake_Cakes_CakeId",
                        column: x => x.CakeId,
                        principalTable: "Cakes",
                        principalColumn: "idCake",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Biscuits_IngredientId",
                table: "Biscuits",
                column: "IngredientId");

            migrationBuilder.CreateIndex(
                name: "IX_CakeCakeBase_CakeId",
                table: "CakeCakeBase",
                column: "CakeId");

            migrationBuilder.CreateIndex(
                name: "IX_CoatingCake_CakeId",
                table: "CoatingCake",
                column: "CakeId");

            migrationBuilder.CreateIndex(
                name: "IX_CreamCake_CakeId",
                table: "CreamCake",
                column: "CakeId");

            migrationBuilder.CreateIndex(
                name: "IX_Creams_IngredientId",
                table: "Creams",
                column: "IngredientId");

            migrationBuilder.CreateIndex(
                name: "IX_DecorCake_CakeId",
                table: "DecorCake",
                column: "CakeId");

            migrationBuilder.CreateIndex(
                name: "IX_Decors_IngredientId",
                table: "Decors",
                column: "IngredientId");

            migrationBuilder.CreateIndex(
                name: "IX_FillingCake_CakeId",
                table: "FillingCake",
                column: "CakeId");

            migrationBuilder.CreateIndex(
                name: "IX_Fruits_IngredientId",
                table: "Fruits",
                column: "IngredientId");

            migrationBuilder.CreateIndex(
                name: "IX_Ingredients_Measureid",
                table: "Ingredients",
                column: "Measureid");

            migrationBuilder.CreateIndex(
                name: "IX_OrderBodies_CakeID",
                table: "OrderBodies",
                column: "CakeID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderBodies_OrderID",
                table: "OrderBodies",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderesProgress_OrderID",
                table: "OrderesProgress",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ClientID",
                table: "Orders",
                column: "ClientID");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_DeliveryID",
                table: "Orders",
                column: "DeliveryID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CakeCakeBase");

            migrationBuilder.DropTable(
                name: "CoatingCake");

            migrationBuilder.DropTable(
                name: "CreamCake");

            migrationBuilder.DropTable(
                name: "DecorCake");

            migrationBuilder.DropTable(
                name: "FillingCake");

            migrationBuilder.DropTable(
                name: "OrderBodies");

            migrationBuilder.DropTable(
                name: "OrderesProgress");

            migrationBuilder.DropTable(
                name: "Biscuits");

            migrationBuilder.DropTable(
                name: "Coatings");

            migrationBuilder.DropTable(
                name: "Creams");

            migrationBuilder.DropTable(
                name: "Decors");

            migrationBuilder.DropTable(
                name: "Fruits");

            migrationBuilder.DropTable(
                name: "Cakes");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Ingredients");

            migrationBuilder.DropTable(
                name: "Clients");

            migrationBuilder.DropTable(
                name: "Deliveries");

            migrationBuilder.DropTable(
                name: "Measures");
        }
    }
}
