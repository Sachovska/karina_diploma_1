﻿using System;
using ShopWebApp.Model.Entities;

namespace ShopWebApp.DataAccessLayer.Interface
{
    public interface IUnitOfWork : IDisposable
    {
        IBaseRepository<cakeBase> BiscuitRepo { get; }
        IBaseRepository<Cake> CakeRepo { get; }
        IBaseRepository<Client> ClientRepo { get; }
        IBaseRepository<cakeCream> CreamRepo { get; }
        IBaseRepository<cakeDecoration> DecorRepo { get; }
        IBaseRepository<Delivery> DeliveryRepo { get; }
        IBaseRepository<cakeFilling> FruitRepo { get; }
        IBaseRepository<Ingredient> IngredientRepo { get; }
        IBaseRepository<Measure> MeasureRepo { get; }
        IBaseRepository<Order> OrderRepo { get; }
        IBaseRepository<OrderBody> OrderBodyRepo { get; }
        IBaseRepository<OrderProgress> OrderProgressRepo { get; }
        IBaseRepository<cakeCoating> CoatingsRepo { get; }
        int Save();
    }
}
