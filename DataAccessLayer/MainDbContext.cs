﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Models.Entities;
using ShopWebApp.Model.Entities;

namespace DAL
{
    public class MainDbContext : DbContext
    {

        public DbSet<cakeBase> Biscuits { get; set; }
        public DbSet<Cake> Cakes { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<cakeCream> Creams { get; set; }
        public DbSet<cakeDecoration> Decors { get; set; }
        public DbSet<Delivery> Deliveries { get; set; }
        public DbSet<cakeFilling> Fruits { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Measure> Measures { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderBody> OrderBodies { get; set; }
        public DbSet<OrderProgress> OrderesProgress { get; set; }
        public DbSet<cakeCoating> Coatings { get; set; }

        public MainDbContext(DbContextOptions<MainDbContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<Cake>()
                .HasKey(b => b.idCake)
                .HasName("PrimaryKey_idCake");
            builder.Entity<CakeCakeBase>()
                .HasKey(k => new { k.CakeBaseId, k.CakeId });
            builder.Entity<CakeCakeBase>()
                .HasOne(t => t.Cake)
                .WithMany(t => t.CakeBase)
                .HasForeignKey(t => t.CakeId);
            builder.Entity<CakeCakeBase>()
                .HasOne(t => t.CakeBase)
                .WithMany(t => t.CakeBase)
                .HasForeignKey(t => t.CakeBaseId);

            builder.Entity<CoatingCake>()
                .HasKey(k => new { k.CakeCoatingId, k.CakeId });
            builder.Entity<CoatingCake>()
                .HasOne(t => t.Cake)
                .WithMany(t => t.Coating)
                .HasForeignKey(t => t.CakeId);
            builder.Entity<CoatingCake>()
                .HasOne(t => t.CakeCoating)
                .WithMany(t => t.Coating)
                .HasForeignKey(t => t.CakeCoatingId);


            builder.Entity<CreamCake>()
                .HasKey(k => new { k.CakeCreamId, k.CakeId });
            builder.Entity<CreamCake>()
                .HasOne(t => t.Cake)
                .WithMany(t => t.Cream)
                .HasForeignKey(t => t.CakeId);
            builder.Entity<CreamCake>()
                .HasOne(t => t.CakeCream)
                .WithMany(t => t.Cream)
                .HasForeignKey(t => t.CakeCreamId);

            builder.Entity<DecorCake>()
                .HasKey(k => new { k.CakeDecorationId, k.CakeId });
            builder.Entity<DecorCake>()
                .HasOne(t => t.Cake)
                .WithMany(t => t.Decor)
                .HasForeignKey(t => t.CakeId);
            builder.Entity<DecorCake>()
                .HasOne(t => t.CakeDecoration)
                .WithMany(t => t.Decor)
                .HasForeignKey(t => t.CakeDecorationId);

            builder.Entity<FillingCake>()
                .HasKey(k => new { k.CakeFillingId, k.CakeId });
            builder.Entity<FillingCake>()
                .HasOne(t => t.Cake)
                .WithMany(t => t.Fruit)
                .HasForeignKey(t => t.CakeId);
            builder.Entity<FillingCake>()
                .HasOne(t => t.CakeFilling)
                .WithMany(t => t.Fruit)
                .HasForeignKey(t => t.CakeFillingId);
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
