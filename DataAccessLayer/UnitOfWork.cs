﻿using System;
using ShopWebApp.DataAccessLayer.Interface;
using ShopWebApp.DataAccessLayer.Repositories;
using ShopWebApp.Model.Entities;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MainDbContext context;
        private IBaseRepository<cakeBase> biscuitRepo;
        private IBaseRepository<Cake> cakeRepo;
        private IBaseRepository<Client> clientRepo;
        private IBaseRepository<cakeCream> creamRepo;
        private IBaseRepository<cakeDecoration> decorRepo;
        private IBaseRepository<Delivery> deliveryRepo;
        private IBaseRepository<cakeFilling> fruitRepo;
        private IBaseRepository<Ingredient> ingredientRepo;
        private IBaseRepository<Measure> measureRepo;
        private IBaseRepository<Order> orderRepo;
        private IBaseRepository<OrderBody> orderBodyRepo;
        private IBaseRepository<OrderProgress> orderProgressRepo;
        private IBaseRepository<cakeCoating> coatingRepo;


        public UnitOfWork(MainDbContext context)
        {
            this.context = context;
        }

        public IBaseRepository<cakeBase> BiscuitRepo
        {
            get
            {
                if (biscuitRepo == null) { biscuitRepo = new BaseRepository<cakeBase>(context); }
                return biscuitRepo;
            }
        }

        public IBaseRepository<Cake> CakeRepo
        {
            get
            {
                if (cakeRepo == null) { cakeRepo = new BaseRepository<Cake>(context); }
                return cakeRepo;
            }
        }

        public IBaseRepository<Client> ClientRepo
        {
            get
            {
                if (clientRepo == null) { clientRepo = new BaseRepository<Client>(context); }
                return clientRepo;
            }
        }

        public IBaseRepository<cakeCream> CreamRepo
        {
            get
            {
                if (creamRepo == null) { creamRepo = new BaseRepository<cakeCream>(context); }
                return creamRepo;
            }
        }

        public IBaseRepository<cakeDecoration> DecorRepo
        {
            get
            {
                if (decorRepo == null) { decorRepo = new BaseRepository<cakeDecoration>(context); }
                return decorRepo;
            }
        }

       
        public IBaseRepository<Delivery> DeliveryRepo
        {
            get
            {
                if (deliveryRepo == null) { deliveryRepo = new BaseRepository<Delivery>(context); }
                return deliveryRepo;
            }
        }

        public IBaseRepository<cakeFilling> FruitRepo
        {
            get
            {
                if (fruitRepo == null) { fruitRepo = new BaseRepository<cakeFilling>(context); }
                return fruitRepo;
            }
        }

        public IBaseRepository<Ingredient> IngredientRepo
        {
            get
            {
                if (ingredientRepo == null) { ingredientRepo = new BaseRepository<Ingredient>(context); }
                return ingredientRepo;
            }
        }

        public IBaseRepository<Measure> MeasureRepo
        {
            get
            {
                if (measureRepo == null) { measureRepo = new BaseRepository<Measure>(context); }
                return measureRepo;
            }
        }

        public IBaseRepository<Order> OrderRepo
        {
            get
            {
                if (orderRepo == null) { orderRepo = new BaseRepository<Order>(context); }
                return orderRepo;
            }
        }

        public IBaseRepository<OrderBody> OrderBodyRepo
        {
            get
            {
                if (orderBodyRepo == null) { orderBodyRepo = new BaseRepository<OrderBody>(context); }
                return orderBodyRepo;
            }
        }

        public IBaseRepository<OrderProgress> OrderProgressRepo
        {
            get
            {
                if (orderProgressRepo == null) { orderProgressRepo = new BaseRepository<OrderProgress>(context); }
                return orderProgressRepo;
            }
        }

        public IBaseRepository<cakeCoating> CoatingsRepo
        {
            get
            {
                if (coatingRepo == null) { coatingRepo = new BaseRepository<cakeCoating>(context); }
                return coatingRepo;
            }
        }

        public int Save()
        {
            return context.SaveChanges();
        }

        private bool isDisposed = false;

        protected virtual void Grind(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            isDisposed = true;
        }

        public void Dispose()
        {
            Grind(true);
            GC.SuppressFinalize(this);
        }
    }
}
