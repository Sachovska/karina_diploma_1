﻿using System.Linq;
using Microsoft.AspNetCore.Identity;
using System;
using DAL;
using ShopWebApp.DataAccessLayer.Interface;
using ShopWebApp.Model.Entities;

namespace ShopWebApp.DataAccessLayer.Seed
{
    public class DbInitializer : IDbInitializer
    {
        private readonly MainDbContext context;
        private readonly IUnitOfWork unitOfWork;

        public DbInitializer(
            MainDbContext context,
            IUnitOfWork unitOfWork)
        {
            this.context = context;
            this.unitOfWork = unitOfWork;
        }

        public void Initialize()
        {
            context.Database.EnsureCreated();


            if (!context.Measures.Any(r => r.Name == "кг"))
            {
                unitOfWork.MeasureRepo.Insert(new Measure
                {
                    Name = "кг",
                    Fullname = "кілограм"
                });
            }

            unitOfWork.Save();

            if (!context.Ingredients.Any(r => r.Name == "молоко"))
            {
                unitOfWork.IngredientRepo.Insert(new Ingredient
                {
                    Name = "молоко",
                    Measureid = unitOfWork.MeasureRepo.Get(n => n.Name == "кг").First().Id
                }) ;
            }
            
            unitOfWork.Save();

            if(!context.Creams.Any(r=>r.name == "Ванільний крем"))
            {
                unitOfWork.CreamRepo.Insert(new cakeCream
                {

                    name = "Ванільний крем",
                    price = 200
                });
            }

            if (!context.Creams.Any(r => r.name == "Шоколадний крем"))
            {
                unitOfWork.CreamRepo.Insert(new cakeCream
                {

                    name = "Шоколадний крем",
                    price = 200
                });
            }

            if (!context.Creams.Any(r => r.name == "Карамельний крем"))
            {
                unitOfWork.CreamRepo.Insert(new cakeCream
                {

                    name = "Карамельний крем",
                    price = 200
                });
            }

            if (!context.Creams.Any(r => r.name == "Вершковий крем"))
            {
                unitOfWork.CreamRepo.Insert(new cakeCream
                {

                    name = "Вершковий крем",
                    price = 200
                });
            }

            if (!context.Creams.Any(r => r.name == "Із вершкового сиру"))
            {
                unitOfWork.CreamRepo.Insert(new cakeCream
                {

                    name = "Із вершкового сиру",
                    price = 200
                });
            }

            if (!context.Creams.Any(r => r.name == "Малиновий крем"))
            {
                unitOfWork.CreamRepo.Insert(new cakeCream
                {

                    name = "Малиновий крем",
                    price = 210
                });
            }

            if (!context.Creams.Any(r => r.name == "Бананово-карамельний"))
            {
                unitOfWork.CreamRepo.Insert(new cakeCream
                {

                    name = "Бананово-карамельний",
                    price = 220
                });
            }

            if (!context.Creams.Any(r => r.name == "Апельсиново-шоколадний"))
            {
                unitOfWork.CreamRepo.Insert(new cakeCream
                {

                    name = "Апельсиново-шоколадний",
                    price = 220
                });
            }

            if (!context.Creams.Any(r => r.name == "Полуничний"))
            {
                unitOfWork.CreamRepo.Insert(new cakeCream
                {

                    name = "Полуничний",
                    price = 220
                });
            }

            if (!context.Creams.Any(r => r.name == "Фісташковий"))
            {
                unitOfWork.CreamRepo.Insert(new cakeCream
                {

                    name = "Фісташковий",
                    price = 250
                });
            }

            unitOfWork.Save();

            if (!context.Biscuits.Any(r => r.name == "Ванільний бісквіт"))
            {
                unitOfWork.BiscuitRepo.Insert(new cakeBase
                {

                    name = "Ванільний бісквіт",
                    price = 100
                });
            }

            if (!context.Biscuits.Any(r => r.name == "Шоколадний бісквіт"))
            {
                unitOfWork.BiscuitRepo.Insert(new cakeBase
                {

                    name = "Шоколадний бісквіт",
                    price = 100
                });
            }

            if (!context.Biscuits.Any(r => r.name == "Карамельний бісквіт"))
            {
                unitOfWork.BiscuitRepo.Insert(new cakeBase
                {

                    name = "Карамельний бісквіт",
                    price = 150
                });
            }

            if (!context.Biscuits.Any(r => r.name == "Малиновий бісквіт"))
            {
                unitOfWork.BiscuitRepo.Insert(new cakeBase
                {

                    name = "Малиновий бісквіт",
                    price = 150
                });
            }

            if (!context.Biscuits.Any(r => r.name == "Горіховий бісквіт"))
            {
                unitOfWork.BiscuitRepo.Insert(new cakeBase
                {

                    name = "Горіховий бісквіт",
                    price = 150
                });
            }

            if (!context.Biscuits.Any(r => r.name == "Бісквіт зі шампанським"))
            {
                unitOfWork.BiscuitRepo.Insert(new cakeBase
                {

                    name = "Бісквіт зі шампанським",
                    price = 160
                });
            }

            unitOfWork.Save();

            if (!context.Fruits.Any(r => r.name == "Малинове желе"))
            {
                unitOfWork.FruitRepo.Insert(new cakeFilling
                {

                    name = "Малинове желе",
                    price = 160
                });
            }

            if (!context.Fruits.Any(r => r.name == "Банановий конфітюр"))
            {
                unitOfWork.FruitRepo.Insert(new cakeFilling
                {

                    name = "Банановий конфітюр",
                    price = 160
                });
            }

            if (!context.Fruits.Any(r => r.name == "Горіховий прошарок"))
            {
                unitOfWork.FruitRepo.Insert(new cakeFilling
                {

                    name = "Горіховий прошарок",
                    price = 160
                });
            }

            if (!context.Fruits.Any(r => r.name == "Ананасовий прошарок"))
            {
                unitOfWork.FruitRepo.Insert(new cakeFilling
                {

                    name = "Ананасовий прошарок",
                    price = 160
                });
            }

            if (!context.Fruits.Any(r => r.name == "Апельсиновий конфітюр"))
            {
                unitOfWork.FruitRepo.Insert(new cakeFilling
                {

                    name = "Апельсиновий конфітюр",
                    price = 90
                });
            }

            if (!context.Fruits.Any(r => r.name == "Прошарок із манго"))
            {
                unitOfWork.FruitRepo.Insert(new cakeFilling
                {

                    name = "Прошарок із манго",
                    price = 150
                });
            }

            if (!context.Fruits.Any(r => r.name == "Банани в карамелі"))
            {
                unitOfWork.FruitRepo.Insert(new cakeFilling
                {

                    name = "Банани в карамелі",
                    price = 120
                });
            }

            if (!context.Fruits.Any(r => r.name == "Шоколадний прошарок"))
            {
                unitOfWork.FruitRepo.Insert(new cakeFilling
                {

                    name = "Шоколадний прошарок",
                    price = 90
                });
            }

            if (!context.Fruits.Any(r => r.name == "Прошарок із чізкейку"))
            {
                unitOfWork.FruitRepo.Insert(new cakeFilling
                {

                    name = "Прошарок із чізкейку",
                    price = 140
                });
            }

            if (!context.Fruits.Any(r => r.name == "Вишневий конфітюр"))
            {
                unitOfWork.FruitRepo.Insert(new cakeFilling
                {

                    name = "Вишневий конфітюр",
                    price = 100
                });
            }

            unitOfWork.Save();

            if (!context.Coatings.Any(r => r.name == "Біла глазур"))
            {
                unitOfWork.CoatingsRepo.Insert(new cakeCoating
                {

                    name = "Біла глазур",
                    price = 90,
                    imgURL = "/data/image/glaze-white.png"
                });
            }

            if (!context.Coatings.Any(r => r.name == "Рожева глазур"))
            {
                unitOfWork.CoatingsRepo.Insert(new cakeCoating
                {

                    name = "Рожева глазур",
                    price = 90,
                    imgURL = "/data/image/glaze-pink.png"
                });
            }

            if (!context.Coatings.Any(r => r.name == "Шоколадна глазур"))
            {
                unitOfWork.CoatingsRepo.Insert(new cakeCoating
                {

                    name = "Шоколадна глазур",
                    price = 90,
                    imgURL = "/data/image/glaze-choco.png"
                });
            }

            if (!context.Coatings.Any(r => r.name == "Покриття з крем-сиру"))
            {
                unitOfWork.CoatingsRepo.Insert(new cakeCoating
                {

                    name = "Покриття з крем-сиру",
                    price = 160,
                    imgURL = "/data/image/creamcheese.png"
                });
            }

            if (!context.Coatings.Any(r => r.name == "Малиновий крем"))
            {
                unitOfWork.CoatingsRepo.Insert(new cakeCoating
                {

                    name = "Малиновий крем",
                    price = 160,
                    imgURL = "/data/image/raspberry.png"
                });
            }

            if (!context.Coatings.Any(r => r.name == "Шоколадний ганаш"))
            {
                unitOfWork.CoatingsRepo.Insert(new cakeCoating
                {

                    name = "Шоколадний ганаш",
                    price = 160,
                    imgURL = "/data/image/choco-ganache.png"
                });
            }

            unitOfWork.Save();

            if (!context.Decors.Any(r => r.name == "Свіжі ягоди"))
            {
                unitOfWork.DecorRepo.Insert(new cakeDecoration
                {

                    name = "Свіжі ягоди",
                    price = 160,
                    imgURL = "/data/image/berries.png"
                });
            }

            if (!context.Decors.Any(r => r.name == "Свіжі фрукти"))
            {
                unitOfWork.DecorRepo.Insert(new cakeDecoration
                {

                    name = "Свіжі фрукти",
                    price = 160,
                    imgURL = "/data/image/fruits.png"
                });
            }

            if (!context.Decors.Any(r => r.name == "Шоколадна стружка"))
            {
                unitOfWork.DecorRepo.Insert(new cakeDecoration
                {

                    name = "Шоколадна стружка",
                    price = 60,
                    imgURL = "/data/image/choco-1.png"
                });
            }

            if (!context.Decors.Any(r => r.name == "Шоколадні фігурки"))
            {
                unitOfWork.DecorRepo.Insert(new cakeDecoration
                {

                    name = "Шоколадні фігурки",
                    price = 100,
                    imgURL = "/data/image/choco-2.png"
                });
            }

            if (!context.Decors.Any(r => r.name == "Мастичні фігурки"))
            {
                unitOfWork.DecorRepo.Insert(new cakeDecoration
                {

                    name = "Мастичні фігурки",
                    price = 160,
                    imgURL = "/data/image/mast.png"
                });
            }

            if (!context.Decors.Any(r => r.name == "Цукрові фігурки"))
            {
                unitOfWork.DecorRepo.Insert(new cakeDecoration
                {

                    name = "Цукрові фігурки",
                    price = 160,
                    imgURL = "/data/image/sugar.png"
                });
            }

            if (!context.Decors.Any(r => r.name == "Цукерки"))
            {
                unitOfWork.DecorRepo.Insert(new cakeDecoration
                {

                    name = "Цукерки",
                    price = 120,
                    imgURL = "/data/image/candies.png"
                });
            }

            if (!context.Decors.Any(r => r.name == "Квіти із крему"))
            {
                unitOfWork.DecorRepo.Insert(new cakeDecoration
                {

                    name = "Квіти із крему",
                    price = 160,
                    imgURL = "/data/image/creamflower.png"
                });
            }

            unitOfWork.Save();

            if (!context.Clients.Any(r => r.Email == "test@gmail.com"))
            {
                unitOfWork.ClientRepo.Insert(new Client
                {
                    Name = "Богдана",
                    Phone = 0440652312,
                    Address = "Чернівці, вул. Головна 23",
                    Email = "test@gmail.com",
                    Login = "test",
                    Password = "test_123",
                    Prepayment = 100                    
                });
            }

            if (!context.Clients.Any(r => r.Email == "test1@gmail.com"))
            {
                unitOfWork.ClientRepo.Insert(new Client
                {
                    Name = "Олег",
                    Phone = 0440652312,
                    Address = "Чернівці, вул. Головна 25",
                    Email = "test1@gmail.com",
                    Login = "test1",
                    Password = "test1_123",
                    Prepayment = 100
                });
            }

            if (!context.Clients.Any(r => r.Email == "test2@gmail.com"))
            {
                unitOfWork.ClientRepo.Insert(new Client
                {
                    Name = "Микола",
                    Phone = 0440652312,
                    Address = "Чернівці, вул. Головна 25",
                    Email = "test2@gmail.com",
                    Login = "test2",
                    Password = "test2_123",
                    Prepayment = 100
                });
            }


            if (!context.Clients.Any(r => r.Email == "test3@gmail.com"))
            {
                unitOfWork.ClientRepo.Insert(new Client
                {
                    Name = "Іван",
                    Phone = 0440652312,
                    Address = "Чернівці, вул. Головна 25",
                    Email = "test3@gmail.com",
                    Login = "test3",
                    Password = "test3_123",
                    Prepayment = 100
                });
            }

            if (!context.Clients.Any(r => r.Email == "test4@gmail.com"))
            {
                unitOfWork.ClientRepo.Insert(new Client
                {
                    Name = "Євген",
                    Phone = 0440652312,
                    Address = "Чернівці, вул. Головна 25",
                    Email = "test4@gmail.com",
                    Login = "test4",
                    Password = "test4_123",
                    Prepayment = 100
                });
            }

            if (!context.Clients.Any(r => r.Email == "test5@gmail.com"))
            {
                unitOfWork.ClientRepo.Insert(new Client
                {
                    Name = "Євген",
                    Phone = 0440652312,
                    Address = "Чернівці, вул. Головна 25",
                    Email = "test5@gmail.com",
                    Login = "test5",
                    Password = "test5_123",
                    Prepayment = 100
                });
            }

            if (!context.Clients.Any(r => r.Email == "test6@gmail.com"))
            {
                unitOfWork.ClientRepo.Insert(new Client
                {
                    Name = "Дмитро",
                    Phone = 0440652312,
                    Address = "Чернівці, вул. Головна 25",
                    Email = "test6@gmail.com",
                    Login = "test6",
                    Password = "test6_123",
                    Prepayment = 100
                });
            }

            if (!context.Clients.Any(r => r.Email == "test7@gmail.com"))
            {
                unitOfWork.ClientRepo.Insert(new Client
                {
                    Name = "Павло",
                    Phone = 0440652312,
                    Address = "Чернівці, вул. Головна 24",
                    Email = "test7@gmail.com",
                    Login = "test7",
                    Password = "test7_123",
                    Prepayment = 100
                });
            }


            unitOfWork.Save();

            if (!context.Deliveries.Any(r => r.Name == "нема"))
            {
                unitOfWork.DeliveryRepo.Insert(new Delivery
                {
                    Name = "нема",
                    Price = 0
                });
            }

            if (!context.Deliveries.Any(r => r.Name == "є"))
            {
                unitOfWork.DeliveryRepo.Insert(new Delivery
                {
                    Name = "є",
                    Price = 50
                });
            }

            unitOfWork.Save();


            if (!context.Orders.Any(r => r.Number == 1))
            {
                unitOfWork.OrderRepo.Insert(new Order
                {
                    Number = 1,
                    ClientID = unitOfWork.ClientRepo.GetAll().First().Id,
                    DeliveryID = unitOfWork.DeliveryRepo.GetAll().First().Id,
                    Dateoforder = DateTime.Now,
                    Dateofdelivery = DateTime.Now,
                    Sum = 120,
                    Prepayment = 50,
                    Prepaymenttrue = true,
                    Discount = 0
                });
            }

            if (!context.Orders.Any(r => r.Number == 2))
            {
                unitOfWork.OrderRepo.Insert(new Order
                {
                    Number = 2,
                    ClientID = unitOfWork.ClientRepo.GetAll().Last().Id,
                    DeliveryID = unitOfWork.DeliveryRepo.GetAll().Last().Id,
                    Dateoforder = DateTime.Now,
                    Dateofdelivery = DateTime.Now,
                    Sum = 100,
                    Prepayment = 0,
                    Prepaymenttrue = false,
                    Discount = 0
                });
            }

            if (!context.Orders.Any(r => r.Number == 3))
            {
                unitOfWork.OrderRepo.Insert(new Order
                {
                    Number = 3,
                    ClientID = unitOfWork.ClientRepo.Get(x=>x.Email == "test6@gmail.com").First().Id,
                    DeliveryID = unitOfWork.DeliveryRepo.GetAll().Last().Id,
                    Dateoforder = DateTime.Now,
                    Dateofdelivery = DateTime.Now,
                    Sum = 100,
                    Prepayment = 0,
                    Prepaymenttrue = false,
                    Discount = 0
                });
            }

            if (!context.Orders.Any(r => r.Number == 4))
            {
                unitOfWork.OrderRepo.Insert(new Order
                {
                    Number = 4,
                    ClientID = unitOfWork.ClientRepo.Get(x => x.Email == "test5@gmail.com").First().Id,
                    DeliveryID = unitOfWork.DeliveryRepo.GetAll().Last().Id,
                    Dateoforder = DateTime.Now,
                    Dateofdelivery = DateTime.Now,
                    Sum = 100,
                    Prepayment = 0,
                    Prepaymenttrue = false,
                    Discount = 0
                });
            }



            if (!context.Orders.Any(r => r.Number == 5))
            {
                unitOfWork.OrderRepo.Insert(new Order
                {
                    Number = 5,
                    ClientID = unitOfWork.ClientRepo.Get(x => x.Email == "test4@gmail.com").First().Id,
                    DeliveryID = unitOfWork.DeliveryRepo.GetAll().Last().Id,
                    Dateoforder = DateTime.Now,
                    Dateofdelivery = DateTime.Now,
                    Sum = 100,
                    Prepayment = 0,
                    Prepaymenttrue = false,
                    Discount = 0
                });
            }


            if (!context.Orders.Any(r => r.Number == 6))
            {
                unitOfWork.OrderRepo.Insert(new Order
                {
                    Number = 6,
                    ClientID = unitOfWork.ClientRepo.Get(x => x.Email == "test7@gmail.com").First().Id,
                    DeliveryID = unitOfWork.DeliveryRepo.GetAll().FirstOrDefault().Id,
                    Dateoforder = DateTime.Now,
                    Dateofdelivery = DateTime.Now,
                    Sum = 150,
                    Prepayment = 150,
                    Prepaymenttrue = false,
                    Discount = 0
                });
            }

            unitOfWork.Save();

            if (!context.Cakes.Any(r => r.nameCake == "Прага"))
            {
                unitOfWork.CakeRepo.Insert(new Cake
                {
                    nameCake = "Прага",
                    aboutCake = "Пражський торт",
                    weightCake = 120,
                    priceCake = 120,
                    imgURL = "https://sladkopuziki.kiev.ua/wp-content/uploads/11-12.jpg",
                    status = "стандартний",
                    dateOfCreating = DateTime.Now
                });
            }

            if (!context.Cakes.Any(r => r.nameCake == "Сметанковий"))
            {
                unitOfWork.CakeRepo.Insert(new Cake
                {
                    nameCake = "Сметанковий",
                    aboutCake = "Сметанковий торт",
                    weightCake = 120,
                    priceCake = 120,
                    imgURL = "https://sladkopuziki.kiev.ua/wp-content/uploads/11-12.jpg",
                    status = "стандартний",
                    dateOfCreating = DateTime.Now
                });
            }

            if (!context.Cakes.Any(r => r.nameCake == "Шарлотка"))
            {
                unitOfWork.CakeRepo.Insert(new Cake
                {
                    nameCake = "Шарлотка",
                    aboutCake = "Яблучний пиріг",
                    weightCake = 120,
                    priceCake = 120,
                    imgURL = "https://sladkopuziki.kiev.ua/wp-content/uploads/11-12.jpg",
                    status = "стандартний",
                    dateOfCreating = DateTime.Now
                });
            }

            unitOfWork.Save();


            if (!context.OrderBodies.Any(r => r.Id == 1))
            {
                unitOfWork.OrderBodyRepo.Insert(new OrderBody
                {
                    OrderID = unitOfWork.OrderRepo.GetAll().Where(t=>t.Number==1).FirstOrDefault().Id,
                    CakeID = unitOfWork.CakeRepo.GetAll().Where(t=>t.nameCake== "Сметанковий").FirstOrDefault().idCake,
                    Amount =1
                });
            }

            if (!context.OrderBodies.Any(r => r.Id == 2))
            {
                unitOfWork.OrderBodyRepo.Insert(new OrderBody
                {
                    OrderID = unitOfWork.OrderRepo.GetAll().Where(t => t.Number == 2).FirstOrDefault().Id,
                    CakeID = unitOfWork.CakeRepo.GetAll().Where(t => t.nameCake == "Шарлотка").FirstOrDefault().idCake,
                    Amount = 1
                });
            }

            if (!context.OrderBodies.Any(r => r.Id == 3))
            {
                unitOfWork.OrderBodyRepo.Insert(new OrderBody
                {
                    OrderID = unitOfWork.OrderRepo.GetAll().Where(t => t.Number == 3).FirstOrDefault().Id,
                    CakeID = unitOfWork.CakeRepo.GetAll().Where(t => t.nameCake == "Прага").FirstOrDefault().idCake,
                    Amount = 1
                });
            }


            if (!context.OrderBodies.Any(r => r.Id == 4))
            {
                unitOfWork.OrderBodyRepo.Insert(new OrderBody
                {
                    OrderID = unitOfWork.OrderRepo.GetAll().Where(t => t.Number == 4).FirstOrDefault().Id,
                    CakeID = unitOfWork.CakeRepo.GetAll().Where(t => t.nameCake == "Прага").FirstOrDefault().idCake,
                    Amount = 1
                });
            }


            if (!context.OrderBodies.Any(r => r.Id == 5))
            {
                unitOfWork.OrderBodyRepo.Insert(new OrderBody
                {
                    OrderID = unitOfWork.OrderRepo.GetAll().Where(t => t.Number == 5).FirstOrDefault().Id,
                    CakeID = unitOfWork.CakeRepo.GetAll().Where(t => t.nameCake == "Прага").FirstOrDefault().idCake,
                    Amount = 1
                });
            }


            if (!context.OrderBodies.Any(r => r.Id == 6))
            {
                unitOfWork.OrderBodyRepo.Insert(new OrderBody
                {
                    OrderID = unitOfWork.OrderRepo.GetAll().Where(t => t.Number == 6).FirstOrDefault().Id,
                    CakeID = unitOfWork.CakeRepo.GetAll().Where(t => t.nameCake == "Шарлотка").FirstOrDefault().idCake,
                    Amount = 1
                });
            }

            unitOfWork.Save();
        }
    }
}
