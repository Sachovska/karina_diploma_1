﻿namespace ShopWebApp.DataAccessLayer.Seed
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
